import React from 'react';
import style from './InputTask.module.css';

export class InputTask extends React.Component {

    constructor(props) {
        super(props);

        this.InputHandler = this.InputHandler.bind(this);
        this.AddNewItem = this.AddNewItem.bind(this);
        this.deleteListItem = this.deleteListItem.bind(this);

        this.state = {
            value: '',
            array: [],
        };
    }

    InputHandler(e) {

        this.setState({
            value: e.target.value,
        });
    }

    AddNewItem(e) {
        if (e.which === 13) {
            if (this.state.value === '') {
                return;
            }

            this.setState({
                value: '',
            });

            this.setState({
                array: [e.target.value, ...this.state.array]
            });
        }

    }

    deleteListItem(e) {

        let text = e.target.parentElement.innerText;

        (() => {

            this.state.array.forEach((item, index) => {
                if(item == text) {
                    this.state.array.splice(index, 1);

                    this.setState({
                        array: this.state.array,
                    });
                }
            });

        })();

    }

    render() {

        return (
            <div className={` ${style.boxinput} `}>
                <input className={` ${style.input} `} type="text" value={this.state.value} onChange={this.InputHandler} onKeyDown={this.AddNewItem} placeholder="Add New Task?" />
                {this.state.array.map((item, id = 0) => (


                    <div key={id++} className={` ${style.taskitem} `}>
                        {item}
                        <span className={`${style.close}`} onClick={this.deleteListItem}></span>
                    </div>
                ))}


            </div>
        );
    }
}


