import React from 'react';
import { InputTask } from './InputTask/InputTask';
import style from './BoxList.module.css';

export class BoxList extends React.Component {

    render(){
        return(
            <div className={` ${style.boxList} `}>
                <InputTask />
            </div>
        );
    }
}