import React from 'react';
import { BoxList } from './BoxList/BoxList';
import style from './BoxList.module.css';
import { Title } from './Title/Title';




export class BoxApp extends React.Component {

    render(){
        return(
            <div className={`${style.app}`}>
                <Title title="To Do" />
                <BoxList />
            </div>
        );
    }
}