import React from 'react';
import './App.css';
import { BoxApp } from './Components/BoxApp';

function App() {
  return (
    <div>
      <BoxApp />
    </div>
  );
}

export default App;
